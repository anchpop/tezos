(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let opcodes_test_contracts_path = "./contracts/opcodes/"

let add_badly () =
  (* `contract` should perhaps be moved to its own file. *)
  let contract =
    {|{
parameter unit;
storage unit;
code
  {
    CAR;

    PUSH int 2; PUSH int 2; ADD; PUSH int 5; ASSERT_CMPEQ;

    # Offset a timestamp by 60 seconds
    PUSH int 60; PUSH timestamp "2019-09-09T12:08:37Z"; ADD;
    PUSH timestamp "2019-09-09T12:09:37Z"; ASSERT_CMPEQ;

    PUSH timestamp "2019-09-09T12:08:37Z"; PUSH int 60; ADD;
    PUSH timestamp "2019-09-09T12:09:37Z"; ASSERT_CMPEQ;

    PUSH mutez 1000; PUSH mutez 1000; ADD;
    PUSH mutez 2000; ASSERT_CMPEQ;

    NIL operation;
    PAIR;
  }
}|}
  in
  Scripts.test_context ()
  >>=? fun ctx ->
  Scripts.run_script ctx contract ~storage:"Unit" ~parameter:"Unit" ()
  >>= function
  | Ok _ ->
      (* The real assertion demonstrating that the operation was correct should in 
      the contract itself, via ASSERT_CMPEQ or similar. *)
      Alcotest.failf
        "ASSERT_CMPEQ failed to cause the contract to evaluate to an error"
  | Error _ ->
      return_unit

let test_contract_runs_without_error filename () =
  let contract_unfixed =
    Scripts.read_file (Filename.concat opcodes_test_contracts_path filename)
  in
  let contract = String.concat "" ["{"; contract_unfixed; "}"] in
  Scripts.test_context ()
  >>=? fun ctx ->
  Scripts.run_script ctx contract ~storage:"Unit" ~parameter:"Unit" ()
  >>= function
  | Ok _ ->
      (* The real assertion demonstrating that the operation was correct should be in the contract itself, via ASSERT_CMPEQ *)
      return_unit
  | Error errs ->
      Alcotest.failf "Unexpected error: %a" Error_monad.pp_print_error errs

let tests =
  [ Test_services.tztest "ASSERT_CMPEQ" `Quick add_badly;
    Test_services.tztest
      "add"
      `Quick
      (test_contract_runs_without_error "add.tz") ]
